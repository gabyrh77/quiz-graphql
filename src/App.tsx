import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Redirect,
  Route
} from "react-router-dom";
import Dashboard from './dashboard';
import Quiz from './quiz';

const App: React.FC = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Redirect to="/dashboard" />
        </Route>
        <Route path="/dashboard">
          <Dashboard />
        </Route>
        <Route path="/quiz/:quiz_id">
          <Quiz />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
