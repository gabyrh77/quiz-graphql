import { gql, useQuery } from '@apollo/client';
import React from 'react';
import { Redirect, Route, Switch, useParams, useRouteMatch } from 'react-router-dom';
import { TQuestion, TQuiz } from '../types';
import Question from './Question';

const QUIZ_QUERY = gql`
query GetQuizQuery($quiz_id: uuid!) {
    quizzes_by_pk(id: $quiz_id) {
      id
      name
      questions {
        id
        text
        options
        answer
      }
    }
  }
`;

type GetQuiz = {
    quizzes_by_pk: TQuiz
}

type QuizRouteProps = {
    url: string
    quizName: string
    question: TQuestion
}

const QuestionRoute: React.FC<QuizRouteProps> = ({ url, question, quizName }) => {
    return (
        <Route path={`${url}/question/${question.id}`}>
            <Question question={question} title={quizName} />
        </Route>
    );
}

type QuizParams = {
    quiz_id: string
    question_id: string
}

const Quiz: React.FC = () => {
    let { quiz_id } = useParams<QuizParams>();
    let { path, url } = useRouteMatch(); 
    const { loading, error, data } = useQuery<GetQuiz>(QUIZ_QUERY, {
        variables: {quiz_id}
    });
    
    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error :(</p>;
    if (!data) return <p>Empty </p>;
    const { name, questions } = data.quizzes_by_pk;

    return (
      <Switch>
        <Route exact path={path}>
           <Redirect to={`${url}/question/${questions[0].id}`} />
        </Route>
        {questions.map((question) => (
          <QuestionRoute key={question.id} url={url} question={question} quizName={name} />
        ))}
      </Switch>
    );
}

export default Quiz;