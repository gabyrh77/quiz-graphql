import { Button, Container, createStyles, FormControl, FormControlLabel, FormHelperText, makeStyles, Radio, RadioGroup, Theme, Typography } from "@material-ui/core";
import React from "react";
import Title from "../components/Title";
import { TQuestion } from "../types";

type QuestionProps = {
    title: string
    question: TQuestion
    nextId?: string
}

const Subtitle: React.FC<{text: string}> = ({text}) => (
    <Typography variant="h5" gutterBottom align="center">
        {text}
    </Typography>
);

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        center: {
            textAlign: "center",
        },
        formControl: {
            paddingTop: theme.spacing(4),
        },
        buttons: {
            '& > *': {
            margin: theme.spacing(1),
            },
            paddingTop: theme.spacing(4),
        },
    })
);

const Question: React.FC<QuestionProps> = ({ title, question, nextId }) => {
    const classes = useStyles();
    const [value, setValue] = React.useState<string>();
    const [error, setError] = React.useState<boolean>(false);
    const [helperText, setHelperText] = React.useState<string>('');
    
    const handleRadioChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setValue((event.target as HTMLInputElement).value);
        setHelperText(' ');
        setError(false);
    };
    
    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
    
        if (value === undefined) {
          setHelperText('Please select an option.');
          setError(true);
        }
    };
    
    return (
        <Container maxWidth="lg">
            <Title text={title} />
            <Subtitle text={question.text} />
            <form className={classes.center} onSubmit={handleSubmit}>
            <FormControl component="fieldset" error={error} className={classes.formControl}>
                <RadioGroup aria-label="quiz" name="quiz" value={value} onChange={handleRadioChange}>
                    {question.options.split(",").map(option => (
                        <FormControlLabel value={option} control={<Radio />} label={option} />
                    ))}
                </RadioGroup>
                <FormHelperText>{helperText}</FormHelperText>
                <div className={classes.buttons}>
                    <Button variant="outlined">Back</Button>
                    <Button type="submit" variant="outlined" color="primary">
                        Next
                    </Button>
                </div>
            </FormControl>
            </form>
        </Container>
    )
}

export default Question;