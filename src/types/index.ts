export type TQuestion = {
    id: string
    text: string
    answer: string
    options: string
}

export type TQuiz = {
    id: string
    name: string
    questions: TQuestion[]
}