import { Typography } from "@material-ui/core";
import React from "react";

const Title: React.FC<{text: string}> = ({ text }) => (
    <Typography variant="h2" gutterBottom align="center">
        {text}
    </Typography>
);

export default Title;