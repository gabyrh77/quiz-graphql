import React from 'react';
import { useQuery, gql } from '@apollo/client';
import { Container, Grid } from '@material-ui/core';
import { TQuiz } from '../types';
import QuizCard from './QuizCard';
import Title from '../components/Title';

const QUIZZES_QUERY = gql`
    query GetQuizzesQuery {
        quizzes {
            id
            name
        }
    }
`;

type GetQuizList = {
    quizzes: TQuiz[]
}

const Dashboard: React.FC = () => {
    const { loading, error, data } = useQuery<GetQuizList>(QUIZZES_QUERY);

    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error :(</p>;
    if (!data) return <p>Empty </p>;
    
    return (
        <Container maxWidth="lg">
            <Title text="Quiz" />
            <Grid container justify="center" spacing={2}>
            {data.quizzes.map(quiz => (
                 <Grid key={quiz.id} item>
                    <QuizCard quiz={quiz} />
                </Grid>
            ))}
            </Grid>
       </Container> 
    );
  }
  
  export default Dashboard;