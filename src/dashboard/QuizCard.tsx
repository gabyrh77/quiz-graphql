import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { TQuiz } from '../types';

const useStyles = makeStyles({
  root: {
    minWidth: 325,
  },
  title: {
    fontSize: 14,
  },
  actions: {
    justifyContent: "end",
  },
});

export type QuizCardProps = {
    quiz: TQuiz
};



const QuizCard: React.FC<QuizCardProps> = ({ quiz }) => {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardContent>
        <Typography variant="h5" component="h2">
          {quiz.name}
        </Typography>
        <Typography color="textSecondary">
          Score
        </Typography>
      </CardContent>
      <CardActions classes={{root: classes.actions}}>
        <Button size="small" component={RouterLink} to={`/quiz/${quiz.id}`}>Start</Button>
      </CardActions>
    </Card>
  );
}

export default QuizCard;
